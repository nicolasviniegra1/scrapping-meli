
class Payload:

    def __init__(self, platform_code, _id, platform_own_id , name, url, createdat, price, currency,\
                type_, image = None, location = None, category = None, sub_category = None,\
                category_url = None, features = None):

        self.platform_code   = platform_code
        self.id              = _id
        self.platform_own_id = platform_own_id
        self.name            = name 
        self.url             = url 
        self.created_at      = createdat 
        # self.date            = date
        self.price           = price
        self.currency        = currency 
        self.type            = type_
        self.image           = image
        self.location        = location
        self.category        = category
        self.sub_category    = sub_category
        self.category_url    = category_url
        self.features        = features


    def print_attributes(self):
        
        print("{")
        print("     Platform Code: ", self.platform_code)
        print("     Id: ", self.id)
        print("     Platform Own Id: ", self.platform_own_id)
        print("     Name: ", self.name)
        print("     Url: ", self.url)
        print("     CreatedAt: ", self.created_at)
        # print("     Date: ", self.date)
        print("     Price: ", self.price)
        print("     Currency: ", self.currency)
        print("     Type: ", self.type)
        print("     Image: ", self.image)
        print("     Location: ", self.location)
        print("     Category: ", self.category)
        print("     Sub Category: ", self.sub_category)
        print("     Category Url: ", self.category_url)
        print("     Features: ", self.features)
        print("}")

    #region Getters

    def get_platform_code(self):
        return self.platform_code

    def get_id(self):
        return self.id

    def get_platform_own_id(self):
        return self.platform_own_id

    def get_name(self):
        return self.name 

    def get_url(self):
        return self.url 

    def get_created_at(self):
        return self.created_at

    # def get_date(self):
    #     return self.date

    def get_price(self):
        return self.price

    def get_currency(self):
        return self.currency

    def get_type(self):
        return self.type

    def get_image(self):
        return self.image

    def get_location(self):
        return self.location

    def get_category(self):
        return self.category

    def get_sub_category(self):
        return self.sub_category
    
    def get_category_url(self):
        return self.category_url

    def get_features(self):
        return self.features

    #endregion

    #region Setters

    def set_platform_code(self, platform_code):
        self.platform_code = platform_code

    def set_id(self, id_):
        self.id = id_

    def set_platform_own_id(self, platform_own_id):
        self.platform_own_id = platform_own_id

    def set_name(self, name):
        self.name = name

    def set_url(self, url):
        self.url = url

    def set_created_at(self, created_at):
        self.created_at = created_at
    
    # def set_date(self, date):
    #     self.date = date

    def set_price(self, price):
        self.price = price

    def set_currency(self, currency):
        self.currency = currency

    def set_type(self, type_):
        self.type = type_

    def set_image(self, image):
        self.image = image

    def set_location(self, location):
        self.location = location

    def set_category(self, category):
        self.category = category

    def set_sub_category(self, sub_category):
        self.sub_category = sub_category

    def set_category_url(self, category_url):
        self.category_url = category_url

    def set_features(self, features):
        self.features = features

    #endregion


    def get_payload_dict(self):

        payload = {
            "PlatformCode" : self.platform_code,
            "Id"           : self.id,
            "PlatformId"   : self.platform_own_id,
            "Name"         : self.name,
            "Url"          : self.url,
            "Price"        : self.price,
            "Currency"     : self.currency,
            "Type"         : self.type,
            "Image"        : self.image,
            "Location"     : self.location,
            "Category"     : self.category,
            "SubCategory"  : self.sub_category,
            "CategoryUrl"  : self.category_url,
            "Features"     : self.features,
            # "Date"         : self.date,
            "CreatedAt"    : self.created_at
        }

        return payload