import time
import requests

def getUrl(url, session, headers = None):
    requestsTimeout = 5
    while True:
        try:
            if headers:
                response = session.get(url, headers=headers, timeout=requestsTimeout)
            else:
                response = session.get(url, timeout=requestsTimeout)
            return response
        except:
            continue
        break
