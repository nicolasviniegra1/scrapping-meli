import os
import time
import requests
import hashlib
from gear.payload         import Payload
from gear.request_methods import getUrl
from bs4                  import BeautifulSoup
from datetime             import datetime

class MercadoLibre:
    def __init__(self, page, mode, country):
        
        self.platform_code = 'ar.mercado_libre'
        self.createdAt  = time.strftime('%Y-%m-%d')
        self.session    = requests.session()
        self.currency   = 'ARS'

        if mode == 'run':
            self.scrape()
        elif mode == 'test':
            self.scrape(test = True)

    def scrape(self, test = False):

        total_count = 0

        payloads = []

        scraped_ids = set()
        scraped_categories = set()

        categories = self.get_categories()

        for category in categories:

            add_filters = self.get_filters(category)
            
            separated_filters = []

            filter_values = {}


            for filter_ in add_filters:

                response = getUrl(filter_, self.session)
                print(response.status_code, response.url)

                soup = BeautifulSoup(response.text, 'html.parser')

                possible_filters = soup.find('div', {'class':'ui-search-search-modal-grid-columns'})

                if not possible_filters:
                    continue

                links_possible_filters = possible_filters.find_all('a', {'class':'ui-search-search-modal-filter ui-search-link'})

                filters_links = []

                first_key_value = filter_[:filter_.index('mercadolibre.com.ar') + 19]
                middle_key_value = category['Url'][filter_.index('mercadolibre.com.ar') + 19:]
                last_key_value = filter_[filter_.index('?filter='):]
                key_category = first_key_value.replace('.mercadolibre.com.ar', '').replace('https://', '')

                key = last_key_value.replace('?filter=', '')

                filter_values[key] = []

                for link in links_possible_filters:

                    link_limpio = link['href'].replace('listado', key_category).replace(first_key_value, '').replace(middle_key_value, '').replace(last_key_value, '').replace('_NoIndex_True', '')
                    if '/' in link_limpio[0]:
                        link_limpio = link_limpio[1:]
                    # print(link_limpio)
                    filters_links.append(link_limpio)
                    filter_values[key].append(link_limpio.replace('/', ''))

                separated_filters.append(filters_links)

            if separated_filters == []:
                mixed_filters = ['']
            elif len(separated_filters) == 1:
                mixed_filters = separated_filters[0]
            else:
                mixed_filters = self.mix_filters('', 0, len(separated_filters), separated_filters, [])


            for filter_ in mixed_filters:

                number_pag = 1

                search_url = category_filter_url = category['Url'] + filter_

                if category_filter_url in scraped_categories:
                    continue

                while True:

                    response = getUrl(search_url, self.session)
                    print(response.status_code, response.url)

                    if response.status_code == 404:
                        break

                    soup = BeautifulSoup(response.text, 'html.parser')

                    if not self.save_products(soup, payloads, scraped_ids, category, filter_values, search_url, category_filter_url):
                        break

                    if not soup.find('div', {'class':'ui-search-pagination'}):
                        break

                    list_more_pages = soup.find('div', {'class':'ui-search-pagination'}).find_all('li')

                    list_more_pages = list_more_pages[1:] if 'Anterior' in list_more_pages[0].text else list_more_pages
                    list_more_pages = list_more_pages[:-1] if 'Siguiente' in list_more_pages[-1].text else list_more_pages

                    if int(list_more_pages[-1].text) > number_pag:
                        for page in list_more_pages:
                            if number_pag + 1 == int(page.text):
                                number_pag += 1
                                search_url = page.find('a')['href']
                                break
                    else:
                        break

                print("Productos / Servicios guardados hasta el momento: {}".format(total_count + len(payloads)))
                print("Categoria actual {} - {}".format(category['FatherCategory'], category['Category']))

                # if len(payloads) >= 50000:
                #     print('Insertados {} productos'.format(len(payloads)))
                #     total_count += 50000


        self.session.close()

        print("Productos / Servicios totales obtenidos: {}".format(total_count + len(payloads)))




    def get_categories(self):

        categories = []

        response = getUrl('https://www.mercadolibre.com.ar/categorias#menu=categories', self.session)
        print(response.status_code, response.url)

        soup = BeautifulSoup(response.text, 'html.parser')

        main_list = soup.find('div', {'class':'categories-container__main'})

        containers = main_list.find_all('div', {'class':'categories__container'})

        for container in containers:

            list_ = container.find_all('li', {'class':'categories__item'})

            for category in list_:

                categories.append(
                    {
                        'FatherCategory': container.find('h2').text,
                        'Category'      : category.find('h3').text ,
                        'Url'           : category.find('a')['href']
                    }
                )

        return categories


    def get_filters(self, category):

        add_filters = []

        response = getUrl(category['Url'], self.session)
        print(response.status_code, response.url)

        soup = BeautifulSoup(response.text, 'html.parser')

        aside = soup.find('aside', {'class':'ui-search-sidebar'})
        try:
            section = aside.find_all('section')[1]
        except:
            section = aside.find('section')

        filter_list = section.find_all('dl')

        for filter_ in filter_list[:-1]:

            sub_filters = filter_.find_all('dd')

            if len(sub_filters) > 4:
                if filter_.find('a', {'class':'ui-search-modal__link ui-search-modal--default ui-search-link'}):
                    add_filters.append(filter_.find('a', {'class':'ui-search-modal__link ui-search-modal--default ui-search-link'})['href'])
                elif filter_.find('dt', {'class':'ui-search-filter-dt-title'}):
                    if 'Categorías' in filter_.find('dt', {'class':'ui-search-filter-dt-title'}).text:
                        add_filters.append(category['Url'] + '_FiltersAvailableSidebar?filter=category')

        return add_filters


    def mix_filters(self, value, index, max_index, separated_filters, mixed_filters):

        if index == max_index:
            return value

        for filter_ in separated_filters[index]:

            mix = value + filter_

            new_filter = self.mix_filters(mix, index + 1, max_index, separated_filters, mixed_filters)

            if index + 1 == max_index:
                mixed_filters.append(new_filter)

        if index == 0:
            return mixed_filters

    
    def save_products(self, soup, payloads, scraped_ids, category, filter_values, search_url, category_filter_url):

        features = []
        location = None

        for filters in filter_values:
            for feature in filter_values[filters]:
                if feature in search_url:
                    if 'state' in filters:
                        location = feature
                    else:
                        features.append(feature)
                    break

        section = soup.find('section', {'class':'ui-search-results'})

        if not section:
            return False

        list_products = section.find_all('li', {'class':'ui-search-layout__item'})

        for product in list_products:

            image_element = product.find('div', {'class': 'ui-search-result__image'})

            a_element = image_element.find('a', {'class':'ui-search-link'})

            price_element = product.find('div', {'class':'ui-search-result__content-wrapper'})

            url        = a_element['href']
            name       = a_element['title']
            try:
                meli_id    = a_element['href'][a_element['href'].index('tracking_id'):].replace('tracking_id=', '')
            except:
                continue
            _id        = hashlib.md5(url.encode('utf-8')).hexdigest()
            image      = a_element.find('img', {'class':'ui-search-result-image__element'})['src']
            price      = int(price_element.find('span', {'class':'price-tag-fraction'}).text.replace('.', ''))
            _type      = 'service' if 'servicios' in category['Url'] else 'product'

            
            if _id not in scraped_ids:
                payload = Payload(self.platform_code, _id, meli_id, name, url, self.createdAt, price, self.currency, _type, image = image, location = location, category = category['FatherCategory'], sub_category = category['Category'], category_url = category_filter_url, features = features)
                scraped_ids.add(_id)
                payloads.append(payload.get_payload_dict())

        return True
