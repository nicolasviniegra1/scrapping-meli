import argparse
import logging
from datetime      import datetime
from sitios.mercado_libre import MercadoLibre

options = ['MercadoLibre']

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    start = datetime.now()
    parser =  argparse.ArgumentParser()

    parser.add_argument('--m',  help = 'Mode', type = str)
    parser.add_argument('--c',  help = 'Country', type = str)
    parser.add_argument('page', help = 'Page to scrape', type = str, choices = options)

    args = parser.parse_args()

    mode       = args.m
    page       = args.page
    country    = args.c

    locals()[page](page, mode, country)

    end = datetime.now()

    print('Total time:', end - start)




